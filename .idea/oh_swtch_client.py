# -*- coding: utf-8-*-
import re
import requests
from urllib2 import urlopen
import xml.etree.ElementTree as etree
from xml.etree import ElementTree

import pprint

SERVER = "192.168.0.105"

class oh_switch_client:
    openhab_host = ""
    openhab_port = ""

    map = {}
    command_table = {}


    def __init__(self,openhab_host="localhost",openhab_port="8080"):
        self.openhab_port = openhab_port
        self.openhab_host = openhab_host
        try :
            test_requests = urlopen("http://%s:%s"%(self.openhab_host,self.openhab_port))
            self.online = True
            self.map = self.get_switch_map()
            self.command_table = self.get_comand_table()
        except:
            self.online = False


    def get_switch_map(self, number_group=0):
        """Funntion get site map and make dict three, where foliages is link to switch"""
        def get_sectors(sectors_group):
            s_map = {}
            for widget in sectors_group.findall("widget"):
                if widget.find("type")!=None:
                    if widget.find("type").text == "Group":
                        s_map[widget.find("label").text.upper()] = get_sectors(widget.find("linkedPage"))
                    elif widget.find("type").text == "Switch":
                        s_map[widget.find("label").text.upper()] = widget.find("item").find("link").text
            return s_map

        response = requests.get("http://%s:%s/rest/sitemaps/demo"%(self.openhab_host,self.openhab_port))
        if not response:
            return {}
        root = ElementTree.fromstring(response.content)
        widgets = root.find("homepage").findall("widget")
        sectors_group = widgets[number_group]
        map = get_sectors(sectors_group)
        return map

    def get_comand_table(self):
        command_list = []
        def get_comand_list_recursion(map={}, pre_line=""):
            """get list of switch and links to one"""
            result = []
            for key, val in map.items():
                if type(val) == type({}):
                    result += get_comand_list_recursion(map=val, pre_line=pre_line+" "+key)
                else:
                    result.append((pre_line+" "+key, val))
            return result

        command_list = get_comand_list_recursion(map=self.map)
        for i in range(len(command_list)):
                buf_name = command_list[i][0]
                buf_name = buf_name.split()
                new_name = []
                for word in buf_name:
                    if not new_name.count(word):
                        new_name.append(word)
                new_name = ' '.join(new_name)
                command_list[i] = (new_name,command_list[i][1])

        command_list = dict(command_list)
        return command_list

    def get_comand_dictionary(self):
        if not self.command_table:
            return None
        comand_dictionary = set()
        for command in self.command_table.keys():
            comand_dictionary = set(command.split()) | comand_dictionary
        comand_dictionary = list(comand_dictionary)
        return comand_dictionary

    def get_state(self,name=""):
        try:
            response = requests.get(self.command_table[name])
            if not response:
                return "Error"
            root = ElementTree.fromstring(response.content)
            state = root.find("state").text
            return state.upper()
        except:
            response = "Error"

    def set_state(self,name="",new_state="ON"):
        try:
            req = requests.put(self.command_table[name]+"/state", data=new_state)
            if req.status_code != requests.codes.ok:
                return "Error"
            else:
                return "Ok"
        except:
            return "Error"

    def get_map_of(self,name=""):
        flag=1
        map = self.map
        result_name = ""
        while flag:
            flag = 0
            for key in map.keys():
                if name[:len(key)]==key:
                    if type(map[key])==type(dict()):
                        map = map[key]
                        name = name[len(key)+1:]
                        result_name += " " + key
                        flag = 1

        if result_name=="":
            return ("MAP OF ROOT IS"+result_name,list(map.keys()))
        else:
            return ("MAP OF "+result_name+"IS",list(map.keys()))






ohc = oh_switch_client(openhab_host=SERVER)
if not ohc.online:
    print("OPEN HAB SERVER OFF")
else:
    WORDS = ohc.get_comand_dictionary() + ["ON", "OF", "STATE", "GET", "MAP"]
    re_string = [ "("+x+")" for x in list(ohc.get_comand_table().keys()) ]
    re_string = "("+"|".join(re_string)+")"+" (ON|OFF|STATE)"
    re_string = "(" + re_string + ")|MAP"
    re_string = r"\b"+ re_string + r"\b"

    def handle(text, mic, profile):
        print("HO HO HO, ITS WORK")

        m = re.search(r"\bMAP\b", text, re.IGNORECASE)
        if bool(m):
            text = text[re.search(r"\b(MAP OFF )|(MAP OF )|(MAP )|(MAP)\b",text, re.IGNORECASE).end():]
            message = ohc.get_map_of(text)
            message = message[0]+" "+', '.join(message[1])
            mic.say(message)
            return

        m = re.search(r"\b(STATE)\b", text, re.IGNORECASE)
        if bool(m):
            text = text[:re.search(r"\b( STATE)\b", text, re.IGNORECASE).start()]
            message = ohc.get_state(name=text)
            if message=="Error":
                mic.say("PLEASE REPEAT")
            else:
                mic.say("STATE OF "+text+" is "+message)
            return

        m = re.search(r"\b(ON)\b", text, re.IGNORECASE)
        if bool(m):
            text = text = text[:re.search(r"\b( ON)( |\Z)", text, re.IGNORECASE).start()]
            message = ohc.set_state(name=text, new_state="ON")
            if message=="Error":
                mic.say("SERVER ERROR")
            else:
                mic.say("THE STATE HAS BEEN CHANGED")
            return

        m = re.search(r"(( OFF)|( OF))( |\Z)", text, re.IGNORECASE)
        if bool(m):
            text = text[:re.search(r"(( OFF)|( OF))( |\Z)", text, re.IGNORECASE).start()]
            print(text)
            message = ohc.set_state(name=text, new_state="OFF")
            if message=="Error":
                mic.say("SERVER ERROR")
            else:
                mic.say("THE STATE HAS BEEN CHANGED")


    def isValid(text):
        return bool(re.search(re_string, text, re.IGNORECASE))

